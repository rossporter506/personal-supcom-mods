name = "SACU RAS Balance"
version = 0.04
copyright = "Copyright � 2018, AgentX506"
description = "Makes Mass Fab farms viable again by halving the mass generated by RAS SACUs"
author = "Agent X506"
url = ""
uid = "411fdba8-a54f-4cda-9510-f62be695610c"

exclusive = false
ui_only = false

