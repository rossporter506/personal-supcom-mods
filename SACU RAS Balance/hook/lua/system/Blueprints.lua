-- Save the original function before we overwrite it with our own.
local OldModBlueprints = ModBlueprints
-- Overwrite the original ModBlueprints
function ModBlueprints(all_blueprints)
    -- first let us call the original ModBlueprints function
    OldModBlueprints(all_blueprints)
    -- now we loop over every UNIT blueprint
    for id,bp in all_blueprints.Unit do
        -- id = init ID. example: URL0103
        -- bp = the whole content of the file URL0103_unit.bp
        -- if we have Categories and RAS inside the unit blueprint then...
        if bp.Categories then
            -- create an array where we can save the unit categories for easier access
            local Categories = {}
            -- loop over unit categories
            for _, cat in bp.Categories do
                -- example: Categories[TECH1] = true -> Categories.TECH1 == true
                Categories[cat] = true
            end
            -- If Support Commander:
            if (Categories.SUBCOMMANDER) and not (Categories.SERAPHIM) then
                -- now halve the RAS mass value inside the blueprint
                bp.Enhancements.ResourceAllocation.ProductionPerSecondMass = bp.Enhancements.ResourceAllocation.ProductionPerSecondMass / 2
                LOG('processing unit >>>'..id..'<<< ('..(bp.Description or 'Unknown')..') new RAS mass rate = "'..bp.Enhancements.ResourceAllocation.ProductionPerSecondMass..'"')
            end
        end
    end
end