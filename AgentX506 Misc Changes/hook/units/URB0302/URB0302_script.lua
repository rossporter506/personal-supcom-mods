#****************************************************************************
#**
#**  File     :  /cdimage/units/URB0302/URB0302_script.lua
#**  Author(s):  David Tomandl
#**
#**  Summary  :  Cybran Tier 3 Air Unit Factory Script
#**
#**  Copyright � 2005 Gas Powered Games, Inc.  All rights reserved.
#****************************************************************************
local CAirFactoryUnit = import('/lua/cybranunits.lua').CAirFactoryUnit

URB0302 = Class(CAirFactoryUnit) {
	PlatformBone = 'B01',
	
	OnStopBuild = function(self, unitBeingBuilt, order )
		CAirFactoryUnit.OnStopBuild(self, unitBeingBuilt, order)
		if not self.FactoryBuildFailed then
			if (self:GetScriptBit('RULEUTC_StealthToggle') == true) then
				if unitBeingBuilt:TestToggleCaps('RULEUTC_StealthToggle') == true then
					unitBeingBuilt:SetScriptBit('RULEUTC_StealthToggle', true)
					unitBeingBuilt:SetMaintenanceConsumptionInactive()
				end
			else
				if unitBeingBuilt:TestToggleCaps('RULEUTC_StealthToggle') == true then
					unitBeingBuilt:SetScriptBit('RULEUTC_StealthToggle', false)
					unitBeingBuilt:SetMaintenanceConsumptionActive()
				end
			end
			unitBeingBuilt:RequestRefreshUI()
		end
	end,
}

TypeClass = URB0302