-- ****************************************************************************
-- **
-- **  File     :  /cdimage/units/XRB0204/XRB0204_script.lua
-- **  Author(s):  Dru Staltman
-- **
-- **  Summary  :  Cybran Engineering tower
-- **
-- **  Copyright © 2005 Gas Powered Games, Inc.  All rights reserved.
-- ****************************************************************************
local CConstructionStructureUnit = import('/lua/cybranunits.lua').CConstructionStructureUnit
local GetDistanceBetweenTwoVectors = import('/lua/utilities.lua').GetDistanceBetweenTwoVectors

XRB0204 = Class(CConstructionStructureUnit)
{
    OnStartBuild = function(self, unitBeingBuilt, order)
        if not self.AnimationManipulator then
            self.AnimationManipulator = CreateAnimator(self)
            self.Trash:Add(self.AnimationManipulator)
        end
        self.AnimationManipulator:PlayAnim(self:GetBlueprint().Display.AnimationOpen, false):SetRate(1)
        CConstructionStructureUnit.OnStartBuild(self, unitBeingBuilt, order)
	end,

	OnStopBeingBuilt = function(self, builder, layer)
		CConstructionStructureUnit.OnStopBeingBuilt(self, builder, layer)
		self.unwantedAssistHandle = self:ForkThread(self.UnwantedAssistThread)
	end,

	otherAssistableWithin = function(parent, maxRadius)
		local position = parent:GetPosition()
		local x1 = position[1] - maxRadius
		local x2 = position[1] + maxRadius
		local z1 = position[3] - maxRadius
		local z2 = position[3] + maxRadius
		local rect = Rect( x1, z1, x2, z2 )
		local units = {}
		local assistables = {}
		
		units = GetUnitsInRect(rect) or {}
		for _,r in units do
			local unitIsFriendly = IsAlly(r:GetArmy(), parent:GetArmy())
			local unitIsConstructing = r:IsUnitState('Building') or r:IsUnitState('SiloBuildingAmmo')
			local unitIsUpgrading = r:IsUnitState('Enhancing') or r:IsUnitState('Upgrading')
			local unitIsRepairable = r:IsIdleState() and (r:GetHealth() < r:GetBlueprint().Defense.MaxHealth)
			
			--LOG('Unit ' .. tostring(r:GetUnitId()) .. ' is friendly:  ' .. tostring(unitIsFriendly) .. ', is constructing: ' .. tostring(unitIsConstructing) .. ', is upgrading: ' .. tostring(unitIsUpgrading)  .. ', is repairable: ' .. tostring(unitIsRepairable))
			
			if unitIsFriendly and (unitIsConstructing or unitIsUpgrading or unitIsRepairable) then
				--LOG('Unit ' .. r:GetUnitId() .. ' is assistable!')
				table.insert(assistables, r)
			end
		end
		
		local distanceToClosestUnit = 1e99
		local closestUnit = assistables[1]
		for _, unit in assistables do
			local testDistance = GetDistanceBetweenTwoVectors(unit:GetPosition(), position)
			--LOG(tostring(testDistance) .. ', ' .. tostring(distanceToClosestUnit) .. ', ' .. tostring(testDistance < distanceToClosestUnit))
			if testDistance < distanceToClosestUnit then
				distanceToClosestUnit = testDistance
				closestUnit = unit
			end
		end
		return closestUnit
	end,

	UnwantedAssistThread = function(self)
		while not self:IsDead() do
			if not self:IsPaused() and (self:GetGuardedUnit() == nil) and (self.UnitBeingBuilt != nil) then
				bp = self.UnitBeingBuilt:GetBlueprint()
				
				local unwantedAssist = bp.CategoriesHash['ANTIMISSILE'] and bp.CategoriesHash['TECH3'] and bp.CategoriesHash['STRUCTURE'] and self.UnitBeingBuilt:IsUnitState('SiloBuildingAmmo')
				
				if unwantedAssist then
					
					local closestAssistableUnit = self.otherAssistableWithin(self, GetDistanceBetweenTwoVectors(self:GetPosition(), self.UnitBeingBuilt:GetPosition()))
					
					if closestAssistableUnit and (closestAssistableUnit != self.UnitBeingBuilt) then
						--LOG('found higher priority assistable')
						IssueClearCommands({self})
					end
				else
					--LOG('not assisting antinuke')
				end
			end
			WaitSeconds(5)
		end
	end,
	
    OnStopBuild = function(self, unitBeingBuilt)
        CConstructionStructureUnit.OnStopBuild(self, unitBeingBuilt)

        if not self.AnimationManipulator then
            self.AnimationManipulator = CreateAnimator(self)
            self.Trash:Add(self.AnimationManipulator)
        end
        self.AnimationManipulator:SetRate(-1)
    end,
}
TypeClass = XRB0204