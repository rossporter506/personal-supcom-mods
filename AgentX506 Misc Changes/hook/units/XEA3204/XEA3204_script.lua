


-----------------------------------------------------------------
-- File     :  /cdimage/units/XEA3204/XEA3204_script.lua
-- Author(s):  Dru Staltman
-- Summary  :  UEF CDR Pod Script
-- Copyright © 2005 Gas Powered Games, Inc.  All rights reserved.
-----------------------------------------------------------------

local TConstructionUnit = import('/lua/terranunits.lua').TConstructionUnit
local GetDistanceBetweenTwoVectors = import('/lua/utilities.lua').GetDistanceBetweenTwoVectors

XEA3204 = Class(TConstructionUnit) {
    OnCreate = function(self)
        TConstructionUnit.OnCreate(self)
        self.docked = true
        self.returning = false
    end,

	OnStopBeingBuilt = function(self, builder, layer)
		TConstructionUnit.OnStopBeingBuilt(self, builder, layer)
		self.unwantedAssistHandle = self:ForkThread(self.UnwantedAssistThread)
	end,

	findClosestAssistableUnitTo = function(parent, maxRadius)
		local position = parent:GetPosition()
		local x1 = position[1] - maxRadius
		local x2 = position[1] + maxRadius
		local z1 = position[3] - maxRadius
		local z2 = position[3] + maxRadius
		local rect = Rect( x1, z1, x2, z2 )
		local units = {}
		local assistables = {}
		
		units = GetUnitsInRect(rect) or {}
		for _,r in units do
			local unitIsFriendly = IsAlly(r:GetArmy(), parent:GetArmy())
			local unitIsConstructing = r:IsUnitState('Building') or r:IsUnitState('SiloBuildingAmmo')
			local unitIsUpgrading = r:IsUnitState('Enhancing') or r:IsUnitState('Upgrading')
			local unitIsRepairable = r:IsIdleState() and (r:GetHealth() < r:GetBlueprint().Defense.MaxHealth)
			
			--LOG('Unit ' .. tostring(r:GetUnitId()) .. ' is friendly:  ' .. tostring(unitIsFriendly) .. ', is constructing: ' .. tostring(unitIsConstructing) .. ', is upgrading: ' .. tostring(unitIsUpgrading)  .. ', is repairable: ' .. tostring(unitIsRepairable))
			
			if unitIsFriendly and (unitIsConstructing or unitIsUpgrading or unitIsRepairable) then
				--LOG('Unit ' .. r:GetUnitId() .. ' is assistable!')
				table.insert(assistables, r)
			end
		end
		
		local distanceToClosestUnit = 1e99
		local closestUnit = assistables[1]
		for _, unit in assistables do
			local testDistance = GetDistanceBetweenTwoVectors(unit:GetPosition(), position)
			--LOG(tostring(testDistance) .. ', ' .. tostring(distanceToClosestUnit) .. ', ' .. tostring(testDistance < distanceToClosestUnit))
			if testDistance < distanceToClosestUnit then
				distanceToClosestUnit = testDistance
				closestUnit = unit
			end
		end
		return closestUnit
	end,

	UnwantedAssistThread = function(self)
		while not self:IsDead() do
			if not self:IsPaused() and (self:GetGuardedUnit() == nil) and (self.UnitBeingBuilt != nil) then
				bp = self.UnitBeingBuilt:GetBlueprint()
				local unwantedAssist = bp.CategoriesHash['ANTIMISSILE'] and bp.CategoriesHash['TECH3'] and bp.CategoriesHash['STRUCTURE'] and self.UnitBeingBuilt:IsUnitState('SiloBuildingAmmo')
				if unwantedAssist then
					--LOG('currently assisting antinuke')
					
					local closestAssistableUnit = self.findClosestAssistableUnitTo(self.Parent, self:GetBlueprint().Economy.MaxBuildDistance)
					
					if closestAssistableUnit and (closestAssistableUnit != self.UnitBeingBuilt) then
						--LOG('found higher priority assistable')
						--LOG(closestAssistableUnit:GetUnitId())
						IssueClearCommands({self})
						IssueStop({self})
						
						if closestAssistableUnit.UnitBeingBuilt then
							IssueMove({self}, closestAssistableUnit.UnitBeingBuilt:GetPosition())
						else
							IssueMove({self}, closestAssistableUnit:GetPosition())
						end
					end
				else
					--LOG('not assisting antinuke')
				end
			end
			WaitSeconds(5)
		end
	end,

    SetParent = function(self, parent, podName)
        self.Parent = parent
        self.PodName = podName
        self:SetCreator(parent)
    end,

    OnKilled = function(self, instigator, type, overkillRatio)
        if self.Parent and not self.Parent.Dead then
            self.Parent:NotifyOfPodDeath(self.PodName)
            self.Parent = nil
        end
        TConstructionUnit.OnKilled(self, instigator, type, overkillRatio)
    end,

    OnStartBuild = function(self, unitBeingBuilt, order)
        TConstructionUnit.OnStartBuild(self, unitBeingBuilt, order)
        self.returning = false
    end,

    OnStopBuild = function(self, unitBuilding)
        TConstructionUnit.OnStopBuild(self, unitBuilding)
		IssueClearCommands({self})
        self.returning = true
    end,

    OnFailedToBuild = function(self)
        TConstructionUnit.OnFailedToBuild(self)
        self.returning = true
    end,

    OnMotionHorzEventChange = function(self, new, old)
        if self and not self.Dead then
            if self.Parent and not self.Parent.Dead then
                local myPosition = self:GetPosition()
                local parentPosition = self.Parent:GetPosition(self.Parent.PodData[self.PodName].PodAttachpoint)
                local distSq = VDist2Sq(myPosition[1], myPosition[3], parentPosition[1], parentPosition[3])
                if self.docked and distSq > 0 and not self.returning then
                    self.docked = false
                    self.Parent:ForkThread(self.Parent.NotifyOfPodStartBuild)
                elseif not self.docked and distSq < 1 and self.returning then
                    self.docked = true
                    self.Parent:ForkThread(self.Parent.NotifyOfPodStopBuild)
                end
            end
        end
    end,

    -- Don't make wreckage
    CreateWreckage = function (self, overkillRatio)
        overkillRatio = 1.1
        TConstructionUnit.CreateWreckage(self, overkillRatio)
    end,
}

TypeClass = XEA3204