-- Save the original function before we overwrite it with our own.
local OldModBlueprints = ModBlueprints
-- Overwrite the original ModBlueprints
function ModBlueprints(all_blueprints)
    -- first let us call the original ModBlueprints function
    OldModBlueprints(all_blueprints)
    -- now we loop over every UNIT blueprint
    for id,bp in all_blueprints.Unit do
        -- id = init ID. example: URL0103
        -- bp = the whole content of the file URL0103_unit.bp
        -- if we have Categories and RAS inside the unit blueprint then...
        if bp.Categories then
            -- create an array where we can save the unit categories for easier access
            local Categories = {}
            -- loop over unit categories
            for _, cat in bp.Categories do
                -- example: Categories[TECH1] = true -> Categories.TECH1 == true
                Categories[cat] = true
            end
			if (Categories.ENGINEER and not Categories.SUBCOMMANDER and not Categories.COMMANDER and not Categories.STRUCTURE and not Categories.POD and not Categories.ENGINEERSTATION) then
				if (Categories.TECH1) then
					bp.Economy.MaxBuildDistance = 6
					LOG('processing unit >>>'..id..'<<< ('..(bp.Description or 'Unknown')..') new max build range = "'..bp.Economy.MaxBuildDistance..'"')
				elseif (Categories.TECH2) then
					bp.Economy.MaxBuildDistance = 7
					LOG('processing unit >>>'..id..'<<< ('..(bp.Description or 'Unknown')..') new max build range = "'..bp.Economy.MaxBuildDistance..'"')
				elseif Categories.TECH3 then
					bp.Economy.MaxBuildDistance = 8
					LOG('processing unit >>>'..id..'<<< ('..(bp.Description or 'Unknown')..') new max build range = "'..bp.Economy.MaxBuildDistance..'"')
				end
				bp.AI.StagingPlatformScanRadius = bp.Economy.MaxBuildDistance
			end
			if (string.sub(id, 3, string.len(id)) == 'b3104') then
				LOG('found T3 radar ' .. id .. '. Making smaller and cheaper...')
				local ratio = math.pow(400,2)/math.pow(600,2)
				-- Adjust build cost (and round to 10)
				bp.Economy.BuildCostEnergy = math.floor(bp.Economy.BuildCostEnergy * ratio/10)*10
				bp.Economy.BuildCostMass = math.floor(bp.Economy.BuildCostMass * ratio/10)*10
				bp.Economy.BuildTime = math.floor(bp.Economy.BuildTime * ratio)
				
				bp.Economy.MaintenanceConsumptionPerSecondEnergy = 900
				bp.Intel.RadarRadius = 400
			end
        end
    end
end