Tooltips['stealth_on_create'] = {
    title = "Toggle Default Stealth",
    description = "Determines whether constructed units start with stealth enabled or disabled.",
}