name = "ArtyMod"
version = 0.04
copyright = "Copyright � 2015, AgentX506"
description = "Attempts to fix the balance of T2 Arty (and fixed T2 TMLs): Notable changes include 25% cheaper, and 50% longer range on, T2 arty; increased min range on both arty and fixed TML. Purely aesthetic changes to UEF and Sera Missile Defense's RoF/damage (same DPS)."
author = "Agent X506"
url = ""
uid = "68792ba7-a3c2-4699-86f8-1277a838ced7"

exclusive = false
ui_only = false

