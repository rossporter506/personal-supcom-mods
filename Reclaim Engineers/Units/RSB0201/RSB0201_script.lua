local SConstructionUnit = import('/lua/seraphimunits.lua').SConstructionUnit
local Buff = import('/lua/sim/Buff.lua')
local EffectTemplate = import('/lua/EffectTemplates.lua')
local AIUtils = import('/lua/AI/aiutilities.lua')
local ScenarioUtils = import('/lua/sim/ScenarioUtilities.lua')
local ScenarioFramework = import('/lua/ScenarioFramework.lua')

RSB0201 = Class(SConstructionUnit) {
    AmbientEffects = 'ST2PowerAmbient',
	
    ReclaimThread = function(self)
		local radius = self:GetBlueprint().Economy.MaxBuildDistance
		local position = self:GetPosition()
		local x1 = position[1] - radius/1.2
		local x2 = position[1] + radius/1.2
		local z1 = position[3] - radius/1.2
		local z2 = position[3] + radius/1.2
		local rect = Rect( x1, z1, x2, z2 )
		local reclaimables = {}
		
		while not self:IsDead() do
			if not self:IsPaused() and self:IsIdleState() then
				reclaimables = GetEntitiesInRect(rect) or {}
				for _,r in reclaimables do
					if r:GetArmy() == -1 then
						IssueReclaim({self}, r)
					end
				end
			end
			WaitSeconds(1)
		end
    end,
    
    OnStopBeingBuilt = function(self, builder, layer)
		SConstructionUnit.OnStopBeingBuilt(self,builder,layer)
        self.ReclaimThreadHandle = self:ForkThread(self.ReclaimThread)
    end,
	OnPaused = function(self)
		IssueClearCommands({self})
	end,
	OnStartBuild = function(self, unitBeingBuilt, order )
		IssueStop({self})
	end,
}
TypeClass = RSB0201




